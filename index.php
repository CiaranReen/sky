<!DOCTYPE html>
<html lang="en">
<head>
    <title>James Bond Channel Guide</title>

    <!-- jQuery CDN -->
    <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
    <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

    <!-- Bootstrap CDN -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

    <!-- bPopup plugin -->
    <script src="assets/js/jquery.bpopup-0.11.0.min.js"></script>

    <!-- Mousewheel plugin -->
    <script type='text/javascript' src='assets/js/mousewheel.js'></script>

    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
</head>
<body>
<div class="container">

<!-- TV scheduler -->
<div class="row">
    <div class="col-md-6">
        <h1>James Bond Channel Guide</h1>
        <div class="scheduler">
            <div class="header-row">
                <div class="header-cell">9AM</div>
                <div class="header-cell">10AM</div>
                <div class="header-cell">11AM</div>
                <div class="header-cell">12PM</div>
                <div class="header-cell">1PM</div>
                <div class="header-cell">2PM</div>
                <div class="header-cell">3PM</div>
                <div class="header-cell">4PM</div>
                <div class="header-cell">5PM</div>
                <div class="header-cell">6PM</div>
                <div class="header-cell">7PM</div>
                <div class="header-cell">8PM</div>
            </div>

            <div class="channels">
                <div class="first_column channel-cell">Channel</div>
                <div class="first_column">Sean Connery</div>
                <div class="first_column">George Lazenby</div>
                <div class="first_column">Roger Moore</div>
                <div class="first_column">Timothy Dalton</div>
                <div class="first_column">Pierce Brosnan</div>
                <div class="first_column">Daniel Craig</div>
            </div>

            <div class="data-row" id="sean_connery">
                <div class="cell sc no-data">No programmes available</div>
            </div>

            <div class="data-row" id="george_lazenby">
                <div class="cell gl no-data">No programmes available</div>
            </div>

            <div class="data-row" id="roger_moore">
                <div class="cell rm no-data">No programmes available</div>
            </div>

            <div class="data-row" id="timothy_dalton">
                <div class="cell td no-data">No programmes available</div>
            </div>

            <div class="data-row" id="pierce_brosnan">
                <div class="cell pb no-data">No programmes available</div>
            </div>

            <div class="data-row" id="daniel_craig">
                <div class="cell dc no-data">No programmes available</div>
            </div>
        </div>
    </div>
</div>

<!-- Element to pop up -->
<div id="programme-info">
    <div class="row">
        <div class="col-md-4">
            <img class="skyLogo" src="assets/images/skyLogo.jpg">
        </div>
        <div class="col-md-8">
            <h3 class="info-header">Remote Record</h3>
        </div>
    </div>
</div>

<!-- Upload Facility -->
<div class="row">
    <div class="col-md-4">
        <h4>Upload New Schedule</h4>
        <form action="" method="post" enctype="multipart/form-data">
            <input type="file" name="schedule">
            <input type="submit" class="btn btn-default" name="submit" value="Update">
        </form>
    </div>
</div>

<!-- Upload file to xml directory -->
<?php
if (isset($_POST['submit'])) {
    $schedule = $_FILES["schedule"]["name"];
    $target_dir = "xml/";
    $target_file = $target_dir . basename($schedule);

    if (move_uploaded_file($_FILES["schedule"]["tmp_name"], $target_file)) {
        echo "The schedule has been uploaded successfully.";
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}

?>

<script>
$(function() {

    $(".scheduler").mousewheel(function(event, delta) {

        this.scrollLeft -= (delta * 30);

        event.preventDefault();

    });

});


// semicolon (;) to ensure closing of earlier scripting
// modal pop up
;(function($) {

    // DOM Ready
    $(function() {

        $('.scheduler').on('click', '.p-link', function(e) {

            // Prevents the default action to be triggered.
            e.preventDefault();

            //get the clicked movie text
            var movie = $(this).text();

            //remove any existing text (if more than 1 has been clicked then they will remain otherwise)
            $('.record-text').remove();
            $('.b-close').remove();

            //populate the pop up
            $('<p class="record-text">The James Bond movie '+ movie +' is set to be recorded</p>').appendTo('#programme-info');
            $('<a class="b-close">Close<a/>').appendTo('#programme-info');

            // Triggering bPopup when click event is fired
            $('#programme-info').bPopup();

        });

    });

})(jQuery);

//insert XML values into DOM
$('document').ready(function() {

    $.ajax({
        type: "GET",
        url: 'xml/schedule.xml',
        dataType: "xml",
        success: function (xml) {

            $(xml).find('sean_channel').each(function(){

                //get the XML data
                var name = $(this).find('name').text();
                var start_time = $(this).find('start_time').text();
                var end_time = $(this).find('end_time').text();

                //get the programme length in hours
                var new_start = start_time.slice(0, -5);
                var new_end = end_time.slice(0, -5);
                var length = new_end - new_start;

                //check if the integer returned is positive, if not then it is a AM/PM calculation
                if (length < 0) {
                    var converted_end = +new_end + 12;
                    length = converted_end - new_start;
                }

                //deal with half hour occurrences
                if (end_time.indexOf("30") >= 0 && start_time.indexOf("30") >= 0) {
                    var new_length = +length * 80;
                } else if (start_time.indexOf("30") >= 0) {
                    //multiply the length in hours by the width of the time slot elements
                    new_length = +length * 80 - 40;
                } else if (end_time.indexOf("30") >= 0) {
                    new_length = +length * 80 + 40;
                } else {
                    new_length = +length * 80;
                }

                //remove existing rows
                $('.sc').remove();

                //append to row
                $('<div class="cell" style="width:'+new_length+'px;"><a class="p-link">'+name+'</a></div>').appendTo('#sean_connery');
            });

            $(xml).find('george_channel').each(function(){

                //get the XML data
                var name = $(this).find('name').text();
                var start_time = $(this).find('start_time').text();
                var end_time = $(this).find('end_time').text();

                //get the programme length in hours
                var new_start = start_time.slice(0, -5);
                var new_end = end_time.slice(0, -5);
                var length = new_end - new_start;

                //check if the integer returned is positive, if not then it is a AM/PM calculation
                if (length < 0) {
                    var converted_end = +new_end + 12;
                    length = converted_end - new_start;
                }

                //deal with half hour occurrences
                if (end_time.indexOf("30") >= 0 && start_time.indexOf("30") >= 0) {
                    var new_length = +length * 80;
                } else if (start_time.indexOf("30") >= 0) {
                    //multiply the length in hours by the width of the time slot elements
                    new_length = +length * 80 - 40;
                } else if (end_time.indexOf("30") >= 0) {
                    new_length = +length * 80 + 40;
                } else {
                    new_length = +length * 80;
                }

                //remove existing rows
                $('.gl').remove();

                //append to row
                $('<div class="cell" style="width:'+new_length+'px;"><a class="p-link">'+name+'</a></div>').appendTo('#george_lazenby');
            });

            $(xml).find('roger_channel').each(function(){

                //get the XML data
                var name = $(this).find('name').text();
                var start_time = $(this).find('start_time').text();
                var end_time = $(this).find('end_time').text();

                //get the programme length in hours
                var new_start = start_time.slice(0, -5);
                var new_end = end_time.slice(0, -5);
                var length = new_end - new_start;

                //check if the integer returned is positive, if not then it is a AM/PM calculation
                if (length < 0) {
                    var converted_end = +new_end + 12;
                    length = converted_end - new_start;
                }

                //deal with half hour occurrences
                if (end_time.indexOf("30") >= 0 && start_time.indexOf("30") >= 0) {
                    var new_length = +length * 80;
                } else if (start_time.indexOf("30") >= 0) {
                    //multiply the length in hours by the width of the time slot elements
                    new_length = +length * 80 - 40;
                } else if (end_time.indexOf("30") >= 0) {
                    new_length = +length * 80 + 40;
                } else {
                    new_length = +length * 80;
                }

                //remove existing rows
                $('.rm').remove();

                //append to row
                $('<div class="cell" style="width:'+new_length+'px;"><a class="p-link">'+name+'</a></div>').appendTo('#roger_moore');
            });

            $(xml).find('timothy_channel').each(function(){

                //get the XML data
                var name = $(this).find('name').text();
                var start_time = $(this).find('start_time').text();
                var end_time = $(this).find('end_time').text();

                //get the programme length in hours
                var new_start = start_time.slice(0, -5);
                var new_end = end_time.slice(0, -5);
                var length = new_end - new_start;

                //check if the integer returned is positive, if not then it is a AM/PM calculation
                if (length < 0) {
                    var converted_end = +new_end + 12;
                    length = converted_end - new_start;
                }

                //deal with half hour occurrences
                if (end_time.indexOf("30") >= 0 && start_time.indexOf("30") >= 0) {
                    var new_length = +length * 80;
                } else if (start_time.indexOf("30") >= 0) {
                    //multiply the length in hours by the width of the time slot elements
                    new_length = +length * 80 - 40;
                } else if (end_time.indexOf("30") >= 0) {
                    new_length = +length * 80 + 40;
                } else {
                    new_length = +length * 80;
                }

                //remove existing rows
                $('.td').remove();

                //append to row
                $('<div class="cell" style="width:'+new_length+'px;"><a class="p-link">'+name+'</a></div>').appendTo('#timothy_dalton');
            });

            $(xml).find('pierce_channel').each(function(){

                //get the XML data
                var name = $(this).find('name').text();
                var start_time = $(this).find('start_time').text();
                var end_time = $(this).find('end_time').text();

                //get the programme length in hours
                var new_start = start_time.slice(0, -5);
                var new_end = end_time.slice(0, -5);
                var length = new_end - new_start;

                //check if the integer returned is positive, if not then it is a AM/PM calculation
                if (length < 0) {
                    var converted_end = +new_end + 12;
                    length = converted_end - new_start;
                }

                //deal with half hour occurrences
                if (end_time.indexOf("30") >= 0 && start_time.indexOf("30") >= 0) {
                    var new_length = +length * 80;
                } else if (start_time.indexOf("30") >= 0) {
                    //multiply the length in hours by the width of the time slot elements
                    new_length = +length * 80 - 40;
                } else if (end_time.indexOf("30") >= 0) {
                    new_length = +length * 80 + 40;
                } else {
                    new_length = +length * 80;
                }

                //remove existing rows
                $('.pb').remove();

                //append to row
                $('<div class="cell" style="width:'+new_length+'px;"><a class="p-link">'+name+'</a></div>').appendTo('#pierce_brosnan');
            });

            $(xml).find('daniel_channel').each(function(){

                //get the XML data
                var name = $(this).find('name').text();
                var start_time = $(this).find('start_time').text();
                var end_time = $(this).find('end_time').text();

                //get the programme length in hours
                var new_start = start_time.slice(0, -5);
                var new_end = end_time.slice(0, -5);
                var length = new_end - new_start;

                //check if the integer returned is positive, if not then it is a AM/PM calculation
                if (length < 0) {
                    var converted_end = +new_end + 12;
                    length = converted_end - new_start;
                }

                //deal with half hour occurrences
                if (end_time.indexOf("30") >= 0 && start_time.indexOf("30") >= 0) {
                    var new_length = +length * 80;
                } else if (start_time.indexOf("30") >= 0) {
                    //multiply the length in hours by the width of the time slot elements
                    new_length = +length * 80 - 40;
                } else if (end_time.indexOf("30") >= 0) {
                    new_length = +length * 80 + 40;
                } else {
                    new_length = +length * 80;
                }

                //remove existing rows
                $('.dc').remove();

                //append to row
                $('<div class="cell" style="width:'+new_length+'px;"><a class="p-link">'+name+'</a></div>').appendTo('#daniel_craig');
            });
        }
    });

});
</script>
</div>
</body>
</html>